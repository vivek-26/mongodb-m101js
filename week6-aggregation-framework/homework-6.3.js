db.companies.aggregate([{
    $match: {
        founded_year: 2004,
        "funding_rounds.4": {
            $exists: true
        }
    }
}, {
    $project: {
        "name": 1,
        averageFunding: {
            $avg: "$funding_rounds.raised_amount"
        },
        _id: 0
    }
}, {
    $sort: {
        averageFunding: -1
    }
}])