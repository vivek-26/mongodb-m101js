db.grades.aggregate([{
        $unwind: "$scores"
    },
    {
        $match: {
            "scores.type": {
                $ne: "quiz"
            }
        }
    },
    {
        $project: {
            "class_id": 1,
            "student_id": 1,
            "scores": 1,
            "_id": 0
        }
    }, {
        $group: {
            _id: {
                classID: "$class_id",
                studentID: "$student_id"
            },
            averageStudentScore: {
                $avg: "$scores.score"
            }
        }
    }, {
        $group: {
            _id: {
                classID: "$_id.classID"
            },
            averageClassScore: {
                $avg: "$averageStudentScore"
            }
        }
    },
    {
        $sort: {
            averageClassScore: -1
        }
    }
])