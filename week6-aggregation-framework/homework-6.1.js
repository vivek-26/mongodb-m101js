db.companies.aggregate([{
        $match: {
            "relationships.person": {
                $ne: null
            }
        }
    },
    {
        $project: {
            name: 1,
            relationships: 1,
            _id: 0
        }
    },
    {
        $unwind: "$relationships"
    },
    {
        $group: {
            _id: "$relationships.person",
            uniqueCompanies: {
                $addToSet: "$name"
            }
        }
    },
    {
        $project: {
            _id: 1,
            count: {
                $size: "$uniqueCompanies"
            }
        }
    },
    {
        $sort: {
            count: -1
        }
    }
])